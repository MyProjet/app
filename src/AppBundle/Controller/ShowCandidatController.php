<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\User\UserInterface;
use UserBundle\Entity\Reponse;

class ShowCandidatController extends Controller
{
    public function showCandidatAction($username)
    {
        $user = $this->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        $em = $this->getDoctrine()->getManager();
        $candidat = $em->getRepository('UserBundle:User')->findOneBy([
            'username' => $username
        ]);

        $score = $em->getRepository(Reponse::class)->findByCandidatScore($candidat->getId());

        return $this->render(':profile:show-profil-candidat.html.twig', array(
            'user' => $user,
            'score' => $score,
            'candidat' => $candidat
        ));


    }

    /*-------------------------------------showCV-----------------------------------------*/

    /**
     *  showCV.
     * @param $candidat
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showCVAction($candidat)
    {
        return $this->render(':profile/showCandidat_Fn:show.html.twig', array(
            'candidat' => $candidat,
        ));
    }

    /**
     *  showCVExperience.
     * @param $candidat
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showCVExperienceAction($candidat)
    {
        $em = $this->getDoctrine()->getManager();
        $cv = $em->getRepository('UserBundle:CV')->findOneBy([
            'userCandidat' => $candidat
        ]);

        $experience = array();
        if ($cv) {
            foreach ($cv->getExperience() as $ex) {
                array_push($experience, $ex);
            }
        }

        return $this->render(':profile/showCandidat_Fn/experiences:show.html.twig', array(
            'experience' => $experience,
        ));
    }

    /**
     *  showCVFormation.
     * @param $candidat
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showCVFormationAction($candidat)
    {
        $em = $this->getDoctrine()->getManager();
        $cv = $em->getRepository('UserBundle:CV')->findOneBy([
            'userCandidat' => $candidat
        ]);
        $formation = array();
        if ($cv) {
            foreach ($cv->getFormation() as $ex) {
                array_push($formation, $ex);
            }
        }
        return $this->render(':profile/showCandidat_Fn/formations:show.html.twig', array(
            'formation' => $formation,
        ));
    }

    /**
     *  showCVProjet.
     * @param $candidat
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showCVProjetAction($candidat)
    {
        $em = $this->getDoctrine()->getManager();
        $cv = $em->getRepository('UserBundle:CV')->findOneBy([
            'userCandidat' => $candidat
        ]);
        $projet = array();
        if ($cv) {
            foreach ($cv->getProjet() as $ex) {
                array_push($projet, $ex);
            }
        }
        return $this->render(':profile/showCandidat_Fn/projets:show.html.twig', array(
            'projet' => $projet,
        ));
    }

    /**
     *  showCVDomaine.
     * @param $candidat
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showCVDomaineAction($candidat)
    {

        $domaine = $candidat->getDomaine();

        return $this->render(':profile/showCandidat_Fn/domaine:show.html.twig', array(
            'domaine' => $domaine,
        ));

    }

    /**
     *  showCVCompetence.
     * @param $candidat
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showCVCompetenceAction($candidat)
    {

        $competence = $candidat->getCV()->getCompetence();

        return $this->render(':profile/showCandidat_Fn/competence:show.html.twig', array(
            'competence' => $competence,
        ));

    }

    /**
     *  showCVLangage.
     * @param $candidat
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showCVLangageAction($candidat)
    {

        $em = $this->getDoctrine()->getManager();
        $cv = $em->getRepository('UserBundle:CV')->findOneBy([
            'userCandidat' => $candidat
        ]);
        if ($cv) {
            $lange = '';
        } else {
            $lange = $cv->getLangue();
        }

        return $this->render(':profile/showCandidat_Fn/langage:show.html.twig', array(
            'lange' => $lange,
        ));

    }

}
