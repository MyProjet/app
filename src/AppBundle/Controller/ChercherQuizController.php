<?php

namespace AppBundle\Controller;

use JMS\Serializer\SerializerBuilder;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\User\UserInterface;

class ChercherQuizController extends Controller
{
    public function showAction()
    {
        return $this->render(':profile/candidat_Fn/quiz:search.html.twig', array());
    }

    /**
     *
     * @Route("/search_quiz", name="search_quiz")
     * @Method("GET")
     *
     * @param Request $request
     * @return Response
     */
    public function searchAction(Request $request)
    {
        $user = $this->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        $em = $this->getDoctrine()->getManager();
        $serializer = SerializerBuilder::create()->build();

        $requestString = $request->get('q');
        $quiz = $em->getRepository('UserBundle:Quiz')->findQuizByString($requestString);

        if (!$quiz || $requestString == null) {
            $result['Quiz']['error'] = "Aucune résulta trouvé";
        } else {
            $result['Quiz'] = $this->getQuiz($quiz);
        }

        $quizList = new Response($serializer->serialize($result, 'json'));
        $quizList->headers->set('Content-Type', 'application/json');

        return $quizList;
    }

    public function getQuiz($quiz)
    {
        $return = array();

        foreach ($quiz as $q) {
            array_push($return, array(
                'Quiz' => $q,
                'icon' => $this->getIconByReponce($q),
            ));
        }

        return $return;
    }

    public function getIconByReponce($quiz)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $return = array();

        $reponse = $em->getRepository('UserBundle:Reponse')->findBy(array(
            'quiz' => $quiz,
            'userCandidat' => $user
        ));

        if ($reponse) {
            foreach ($reponse as $r) {
                if (($r->getUserCandidat() == $user && $r->getSuccess() == true) ||
                    ($r->getUserCandidat() == $user && $this->checkDate($r->getDate()))
                ) {
                    $return['name'] = 'checkmark4';
                    $return['color'] = 'success';
                } else {
                    $return['name'] = 'blocked';
                    $return['color'] = 'danger';
                }

            }
        } else {
            $return['name'] = 'exclamation ';
            $return['color'] = 'info';
        }

        return $return;
    }

    public function checkDate($dates)
    {
        $now = time();
        $date = strtotime($dates->format('Y-m-d'));
        $compare = $now - $date;
        $return = false;

        if ((floor($compare / (60 * 60 * 24))) >= 15) {
            $return = true;
        }

        return $return;
    }

}
