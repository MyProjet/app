<?php

namespace AppBundle\Controller;

use JMS\Serializer\SerializerBuilder;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\User\UserInterface;
use UserBundle\Entity\Reponse;
use UserBundle\Entity\UserCandidat;

class ReponceQuizController extends Controller
{
    /**
     *
     * @Route("/quiz/{id}", name="quiz_show", requirements={"id": "\d+"})
     * @param Request $request
     * @param $id
     * @return Response
     */
    public function showQuizAction(Request $request, $id)
    {
        $user = $this->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        if (!$user->hasRole('ROLE_CANDIDAT')) {
            return $this->redirectToRoute('homepage');
        }

        $em = $this->getDoctrine()->getManager();

        $quiz = $em->getRepository('UserBundle:Quiz')->find($id);
        $reponce = $em->getRepository('UserBundle:Reponse')->findByQuiz($quiz);

        $question = array();
        foreach ($quiz->getQuestion() as $qz) {
            array_push($question, $qz);
        }

        /*vérifier si le quiz est bien passée */
        foreach ($reponce as $r) {
            if (($r->getUserCandidat() == $user && $r->getQuiz() == $quiz && $r->getSuccess() == true) ||
                (($r->getUserCandidat() == $user && $r->getQuiz() == $quiz) && !$this->checkDate($r->getDate()))
            ) {
                return $this->forward('AppBundle:ReponceQuiz:showQuizReponce', array(
                    'myReponce' => $r->getMyreponse(),
                    'question' => $question,
                    'quiz' => $quiz,
                    'date' => $this->dateReste($r->getDate()),
                    'reponce' => $r->getSuccess(),
                ));
            }
        }

        $form = $this->createFormBuilder()
            ->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $reponce = new Reponse();

            $myReponce = $request->request->all();
            $q = $this->validation($myReponce, $id);
            $s = $this->score($myReponce, $id);

            $reponce->setQuiz($quiz);
            $reponce->setUserCandidat($user);
            $reponce->setSuccess($q);
            $reponce->setMyreponse($myReponce);

            if ($q) {
                $reponce->setScore($s);
            } else {
                $reponce->setScore(0);
            }

            $reponce->setDate(new \DateTime(date('Y-m-d')));

            $em->persist($reponce);
            $em->flush();


            return $this->forward('AppBundle:ReponceQuiz:showQuizReponce', array(
                'myReponce' => $myReponce,
                'question' => $question,
                'quiz' => $quiz,
            ));
        }


        return $this->render(':profile/candidat_Fn/quiz:show.html.twig', array(
            'user' => $user,
            'quiz' => $quiz,
            'question' => $question,
            'form' => $form->createView(),
        ));

    }

    public function checkDate($dates)
    {
        $now = time();
        $date = strtotime($dates->format('Y-m-d'));
        $compare = $now - $date;
        $return = false;

        if ((floor($compare / (60 * 60 * 24))) >= 15) {
            $return = true;
        }

        return $return;
    }

    public function dateReste($dates)
    {
        $now = time();
        $date = strtotime($dates->format('Y-m-d'));
        $compare = $now - $date;

        $return = (15 - (floor($compare / (60 * 60 * 24))));

        return $return;
    }

    public function validation($myReponce, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $qestion = $em->getRepository('UserBundle:Question')->findByQuiz($id);

        $rs = array();
        foreach ($myReponce as $r) {
            array_push($rs, $r);
        }

        $qs = array();
        foreach ($qestion as $q) {
            foreach ($q as $qq) {
                array_push($qs, $qq);
            }
        }

        $result = false;
        for ($i = 0; $i < count($qs); $i++) {
            if ($qs[$i] != $rs[$i]) {
                $result = false;
                break;
            } else {
                $result = true;
            }
        }

        return $result;
    }

    public function score($myReponce, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $qestion = $em->getRepository('UserBundle:Question')->findByQuiz($id);

        $rs = array();
        foreach ($myReponce as $r) {
            array_push($rs, $r);
        }

        $qs = array();
        foreach ($qestion as $q) {
            foreach ($q as $qq) {
                array_push($qs, $qq);
            }
        }

        $score = 1.61803398875; //nombre d'or
        $nbReponce = 0;
        for ($i = 0; $i < count($qs); $i++) {
            try {
                if ($qs[$i] == $rs[$i]) {
                    $score = $score * 1.61803398875;
                    $nbReponce = $nbReponce + 1;
                }
            } catch (\Exception $e) {
                break;
            }
        }

        $result = 0;
        if ($nbReponce >= ((count($qs) / 2) + 1)) {
            $result = round($score);
        }

        return $result;
    }

    /**
     *
     * @param Request $request
     * @return Response
     */
    public function showQuizReponceAction(Request $request)
    {
        $user = $this->getUser();
        $myReponce = $request->get('myReponce');
        $question = $request->get('question');
        $quiz = $request->get('quiz');
        $date = $request->get('date');
        $reponce = $request->get('reponce');

        return $this->render(':profile/candidat_Fn/quiz:result.html.twig', array(
            'user' => $user,
            'quiz' => $quiz,
            'question' => $question,
            'myReponce' => $myReponce,
            'date' => $date,
            'reponce' => $reponce,
        ));
    }

    /**
     *
     * @Route("/reponce/all", name="all_reponce_quiz")
     * @Method("GET")
     *
     * @param Request $request
     * @return Response
     */
    public function allAction(Request $request)
    {
        $user = $this->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        $em = $this->getDoctrine()->getManager();
        $serializer = SerializerBuilder::create()->build();

        $result = $em->getRepository(Reponse::class)->findAll();

        $response = new Response($serializer->serialize($result, 'json'));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     *
     * @Route("/reponce/etapes/{id}", name="reponce_etapes")
     * @Method("GET")
     *
     * @param Request $request
     * @param UserCandidat $candidat
     * @return Response
     */
    public function reponceAction(Request $request, UserCandidat $candidat)
    {

        $user = $this->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        $result = [];

        $em = $this->getDoctrine()->getManager();
        $serializer = SerializerBuilder::create()->build();

        if ($candidat) {
            if ($user == $candidat) {
                $result = $em->getRepository(Reponse::class)->findByCandidatEtaps($user->getId());
            } else {
                $result = $em->getRepository(Reponse::class)->findByCandidatEtaps($candidat->getId());
            }
        }

        $response = new Response($serializer->serialize($result, 'json'));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }
}
