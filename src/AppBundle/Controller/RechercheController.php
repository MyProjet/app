<?php

namespace AppBundle\Controller;

use JMS\Serializer\SerializerBuilder;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\User\UserInterface;
use UserBundle\Entity\Reponse;
use UserBundle\Entity\UserCandidat;
use UserBundle\Entity\UserRecruteur;

class RechercheController extends Controller
{

    /**
     * @Route("/recherche", name="recherche")
     * @return Response
     * @internal param Request $request
     */
    public function rechercheAction()
    {
        $user = $this->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        if ($user->hasRole('ROLE_CANDIDAT')) {
            $parent_template_var = 'profile/candidat_Fn/recherche.html.twig';
        } elseif ($user->hasRole('ROLE_RECRUTEUR')) {
            $parent_template_var = 'profile/recruteur_Fn/recherche.html.twig';
        } else {
            return $this->redirectToRoute('homepage');
        }

        return $this->render($parent_template_var, [
            'user' => $user,
        ]);
    }

    /**
     * @Route("/chercher_candidat", name="chercher_candidat")
     * @param Request $request
     * @return Response
     */
    public function chercherCandidatAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $serializer = SerializerBuilder::create()->build();
        $requestString = $request->get('q');

        $requestString = preg_replace('/\s+/', ' ', $requestString);
        $requestArray = str_word_count($requestString, 1, 'àáãçéè');
        $resultCandidat = array();
        if ($requestString) {

            //By Domaine
            $CandidatDomaine = array();
            foreach ($requestArray as $r) {
                $Candidat = $em->getRepository(UserCandidat::class)->findByDomaine($r);

                if ($Candidat) {
                    foreach ($Candidat as $C) {
                        array_push($CandidatDomaine, $C);
                    }
                }
            }

            //By Competence
            $CandidatCompetence = array();
            foreach ($requestArray as $r) {
                $Candidat = $em->getRepository(UserCandidat::class)->findByCompetence($r);

                if ($Candidat) {
                    foreach ($Candidat as $C) {
                        array_push($CandidatCompetence, $C);
                    }
                }
            }
            //return tout les candidat son repetition
            $Candidat = array_unique($CandidatDomaine + $CandidatCompetence);

            /*--------------------------------------------------------------------------*/

            $Reponse = $em->getRepository(Reponse::class)->findByCandidatReponse($Candidat);

            $resultCandidat = $this->getCandidatByScore($Candidat, $Reponse);

        }
        $response = new Response($serializer->serialize($resultCandidat, 'json'));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    protected function getCandidatByScore($Candidat, $Reponse)
    {
        $return = array();
        foreach ($Candidat as $cdn) {
            $verify = false;
            foreach ($Reponse as $rep) {
                foreach ($rep as $re) {
                    if ($re->getScore() > 0 && $re->getUserCandidat() == $cdn) {
                        $verify = true;
                    }
                }
            }
            if ($verify) {
                array_push($return, $cdn);
            }
        }
        return $this->getCandidat($return);;

    }


    public function getCandidat($resultCandidat)
    {
        usort($resultCandidat, array($this, "order"));
        $return = array();
        foreach ($resultCandidat as $rc) {
            if ($rc->getImage()) {
                $helper = $this->container->get('vich_uploader.templating.helper.uploader_helper');
                $path = $helper->asset($rc->getImage(), 'imageFile');
            } else {
                $path = '/images/man.png';
            }
            array_push($return, array(
                'Candidat' => $rc,
                'imgUrl' => $path
            ));
        }
        return $return;
    }

    function order($a, $b)
    {
        $return = false;
        if ($this->sommeScore($b) >= $this->sommeScore($a)) {
            $return = true;
        }
        return $return;
    }

    public function sommeScore($result)
    {
        $em = $this->getDoctrine()->getManager();
        $score = $em->getRepository(Reponse::class)->findByCandidatScore($result->getId());
        return $score;
    }

    /**
     * @Route("/chercher_recruteur", name="chercher_recruteur")
     * @param Request $request
     * @return Response
     */
    public function chercherRecruteurAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $serializer = SerializerBuilder::create()->build();

        $requestString = $request->get('q');
        $Recruteur = $em->getRepository(UserRecruteur::class)->findRecByDomaine($requestString);

        $result = $this->getRecruteur($Recruteur);
        $response = new Response($serializer->serialize($result, 'json'));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    protected function getRecruteur($Recruteur)
    {
        $return = array();
        foreach ($Recruteur as $rec) {
            if ($rec->getImage()) {
                $helper = $this->container->get('vich_uploader.templating.helper.uploader_helper');
                $path = $helper->asset($rec->getImage(), 'imageFile');
            } else {
                $path = '/images/man.png';
            }
            array_push($return, array(
                'Recruteur' => $rec,
                'imgUrl' => $path
            ));
        }
        return $return;

    }
}
