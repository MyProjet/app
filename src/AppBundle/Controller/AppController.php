<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class AppController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @return \Symfony\Component\HttpFoundation\Response
     * @internal param Request $request
     */
    public function indexAction()
    {
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();

        $quiz = $em->getRepository('UserBundle:Quiz')->findAll();

        $candidat = $em->getRepository('UserBundle:UserCandidat')->findAll();
        $recruteur = $em->getRepository('UserBundle:UserRecruteur')->findAll();

        return $this->render('app/index.html.twig', [
            'user' => $user,
            'quiz' => $quiz,
            'candidat' => $candidat,
            'recruteur' => $recruteur,
        ]);
    }

    /**
     * @Route("/apropos", name="apropos")
     * @return Response
     * @internal param Request $request
     */
    public function aproposAction()
    {
        $user = $this->getUser();
        return $this->render('app/apropos.html.twig', [
            'user' => $user,
        ]);
    }
}
