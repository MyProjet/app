<?php

namespace AppBundle\Controller;

use JMS\Serializer\SerializerBuilder;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\User\UserInterface;
use UserBundle\Entity\CV;
use UserBundle\Entity\Experience;
use UserBundle\Entity\Formation;
use UserBundle\Entity\Projet;
use UserBundle\Form\CVType;
use UserBundle\Form\ExperienceType;
use UserBundle\Form\FormationType;
use UserBundle\Form\ProjetType;
use UserBundle\Form\UserCandidatType;

class CandidatController extends Controller
{
    /*-------------------------------------showCV-----------------------------------------*/

    /**
     *  showCV.
     */
    public function showCVAction()
    {
        $user = $this->getUser();
        return $this->render(':profile/candidat_Fn/cv:cv.html.twig', array(
            'user' => $user,
        ));
    }

    /**
     *  showCVExperience.
     */
    public function showCVExperienceAction()
    {
        $user = $this->getUser();
        $experience = array();
        if ($user->getCV()) {
            foreach ($user->getCV()->getExperience() as $ex) {
                array_push($experience, $ex);
            }
        }

        return $this->render(':profile/candidat_Fn/cv/experiences:show.html.twig', array(
            'user' => $user,
            'experience' => $experience,
        ));
    }

    /**
     *  showCVFormation.
     */
    public function showCVFormationAction()
    {
        $user = $this->getUser();
        $formation = array();
        if ($user->getCV()) {
            foreach ($user->getCV()->getFormation() as $ex) {
                array_push($formation, $ex);
            }
        }
        return $this->render(':profile/candidat_Fn/cv/formations:show.html.twig', array(
            'user' => $user,
            'formation' => $formation,
        ));
    }

    /**
     *  showCVProjet.
     */
    public function showCVProjetAction()
    {
        $user = $this->getUser();
        $projet = array();
        if ($user->getCV()) {
            foreach ($user->getCV()->getProjet() as $ex) {
                array_push($projet, $ex);
            }
        }
        return $this->render(':profile/candidat_Fn/cv/projets:show.html.twig', array(
            'user' => $user,
            'projet' => $projet,
        ));
    }

    /**
     *  showCVDomaine.
     */
    public function showCVDomaineAction()
    {
        $user = $this->getUser();
        $domaine = $user->getDomaine();

        return $this->render(':profile/candidat_Fn/cv/domaine:show.html.twig', array(
            'user' => $user,
            'domaine' => $domaine,
        ));

    }

    /**
     *  showCVLangage.
     */
    public function showCVLangageAction()
    {
        $user = $this->getUser();
        if (!$user->getCv()) {
            $lange = '';
        } else {
            $lange = $user->getCv()->getLangue();
        }

        return $this->render(':profile/candidat_Fn/cv/langage:show.html.twig', array(
            'user' => $user,
            'lange' => $lange,
        ));

    }

    public function showCVCompetenceAction()
    {
        $user = $this->getUser();
        if (!$user->getCv()) {
            $competence = '';
        } else {
            $competence = $user->getCv()->getCompetence();
        }

        return $this->render(':profile/candidat_Fn/cv/competence:show.html.twig', array(
            'user' => $user,
            'competence' => $competence,
        ));

    }


    /*----------------------------------addCV-----------------------------------------*/

    /**
     *  addCV.
     */
    public function addCVAction()
    {
        $user = $this->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        if ($user->getCV() == null) {
            $cv = new CV();
            $cv->setUserCandidat($user);
            $user->setCV($cv);

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
        }

        return $this->render(':profile/candidat_Fn/cv:add.html.twig', array(
            'user' => $user,
        ));
    }

    /**
     *  addCVExperience.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function addCVExperienceAction(Request $request)
    {
        $user = $this->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        $experience = new Experience();
        $experience->setCV($user->getCV());


        $form = $this->createForm(ExperienceType::class, $experience);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($experience);
            $em->flush();

            return $this->redirectToRoute('fos_user_profile_show');
        }

        return $this->render(':profile/candidat_Fn/cv/experiences:add.html.twig', array(
            'user' => $user,
            'form' => $form->createView(),
        ));
    }

    /**
     *  addCVFormation.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function addCVFormationAction(Request $request)
    {
        $user = $this->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        $formation = new Formation();
        $formation->setCV($user->getCV());

        $form = $this->createForm(FormationType::class, $formation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($formation);
            $em->flush();

            return $this->redirectToRoute('fos_user_profile_show');
        }

        return $this->render(':profile/candidat_Fn/cv/formations:add.html.twig', array(
            'user' => $user,
            'form' => $form->createView(),
        ));
    }

    /**
     *  addCVProjet.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function addCVProjetAction(Request $request)
    {
        $user = $this->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        $projet = new Projet();
        $projet->setCV($user->getCV());

        $form = $this->createForm(ProjetType::class, $projet);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($projet);
            $em->flush();

            return $this->redirectToRoute('fos_user_profile_show');
        }

        return $this->render(':profile/candidat_Fn/cv/projets:add.html.twig', array(
            'user' => $user,
            'form' => $form->createView(),
        ));
    }

    public function addCVDomaineAction(Request $request)
    {
        $user = $this->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        $form = $this->createForm(UserCandidatType::class, $user, array(
            'mode' => 'domaine'
        ));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('fos_user_profile_show');
        }

        return $this->render(':profile/candidat_Fn/cv/domaine:add.html.twig', array(
            'user' => $user,
            'form' => $form->createView(),
        ));

    }

    public function addCVCompetenceAction(Request $request)
    {
        $user = $this->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        $form = $this->createForm(UserCandidatType::class, $user, array(
            'mode' => 'cv',
            'model' => 'competence'
        ));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('fos_user_profile_show');
        }

        return $this->render(':profile/candidat_Fn/cv/competence:add.html.twig', array(
            'user' => $user,
            'form' => $form->createView(),
        ));

    }

    public function addCVLangageAction(Request $request)
    {
        $user = $this->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        $form = $this->createForm(CVType::class, $user->getCv(), array(
            'model' => 'langue'
        ));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($user->getCv());
            $em->flush();

            return $this->redirectToRoute('fos_user_profile_show');
        }

        return $this->render(':profile/candidat_Fn/cv/langage:add.html.twig', array(
            'user' => $user,
            'form' => $form->createView(),
        ));


    }


    /*----------------------------------dellCV-----------------------------------------*/

    /**
     *  addCVExperience.
     *
     * @Route("/dellExperience/{id}", name="dell-experience")
     * @param $id
     * @return Response
     */
    public function dellCVExperienceAction($id)
    {
        $user = $this->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        $em = $this->getDoctrine()->getManager();
        $serializer = SerializerBuilder::create()->build();
        $experience = $em->find('UserBundle:Experience', $id);

        if ($experience && $user == $experience->getCV()->getUserCandidat()) {
            try {
                $em->remove($experience);
                $em->flush();
                $rep = true;
            } catch (\Exception $e) {
                $rep = false;
            }
        } else {
            $rep = false;
        }

        $response = new Response($serializer->serialize($rep, 'json'));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     *  dellCVFormation.
     * @Route("/dellFormation/{id}", name="dell-formation")
     * @param $id
     * @return Response
     */
    public function dellCVFormationAction($id)
    {
        $user = $this->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        $em = $this->getDoctrine()->getManager();
        $serializer = SerializerBuilder::create()->build();
        $formation = $em->find('UserBundle:Formation', $id);

        if ($formation && $user == $formation->getCV()->getUserCandidat()) {
            try {
                $em->remove($formation);
                $em->flush();
                $rep = true;
            } catch (\Exception $e) {
                $rep = false;
            }
        } else {
            $rep = false;
        }

        $response = new Response($serializer->serialize($rep, 'json'));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     *  dellCVProjet.
     * @Route("/dellProjet/{id}", name="dell-projet")
     * @param $id
     * @return Response
     */
    public function dellCVProjetAction($id)
    {
        $user = $this->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        $em = $this->getDoctrine()->getManager();
        $serializer = SerializerBuilder::create()->build();
        $projet = $em->find('UserBundle:Projet', $id);

        if ($projet && $user == $projet->getCV()->getUserCandidat()) {
            try {
                $em->remove($projet);
                $em->flush();
                $rep = true;
            } catch (\Exception $e) {
                $rep = $e;
            }
        } else {
            $rep = false;
        }

        $response = new Response($serializer->serialize($rep, 'json'));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }


    /*----------------------------------updateCV-----------------------------------------*/

    /**
     *  updateCVExperience.
     * @Route("/updateCVExperience/{id}", name="updateCVExperience")
     * @param Request $request
     * @param $experience
     * @return Response
     */
    public function updateCVExperienceAction(Request $request, Experience $experience)
    {

        $form = $this->createForm(ExperienceType::class, $experience);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($experience);
            $em->flush();

            return $this->redirectToRoute('fos_user_profile_show');
        }

        return $this->render(':profile/candidat_Fn/cv/experiences:update.html.twig', array(
            'form' => $form->createView(),
            'experience' => $experience,
        ));
    }

    /**
     *  updateCVFormation.
     * @Route("/updateCVFormation/{id}", name="updateCVFormation")
     * @param Request $request
     * @param Formation $formation
     * @return Response
     */
    public function updateCVFormationAction(Request $request, Formation $formation)
    {
        $form = $this->createForm(FormationType::class, $formation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($formation);
            $em->flush();

            return $this->redirectToRoute('fos_user_profile_show');
        }

        return $this->render(':profile/candidat_Fn/cv/formations:update.html.twig', array(
            'form' => $form->createView(),
            'formation' => $formation,
        ));
    }

    /**
     *  updateCVProjet.
     * @Route("/updateCVProjet/{id}", name="updateCVProjet")
     * @param Request $request
     * @param Projet $projet
     * @return Response
     */
    public function updateCVProjetAction(Request $request, Projet $projet)
    {
        $form = $this->createForm(ProjetType::class, $projet);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($projet);
            $em->flush();

            return $this->redirectToRoute('fos_user_profile_show');
        }

        return $this->render(':profile/candidat_Fn/cv/projets:update.html.twig', array(
            'form' => $form->createView(),
            'projet' => $projet,
        ));
    }

}
