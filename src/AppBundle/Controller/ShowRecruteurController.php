<?php

namespace AppBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\User\UserInterface;

class ShowRecruteurController extends Controller
{

    public function showRecruteurAction($username)
    {
        $user = $this->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        $em = $this->getDoctrine()->getManager();
        $recruteur = $em->getRepository('UserBundle:User')->findOneBy([
            'username' => $username
        ]);

        return $this->render(':profile:show-profil-recuteur.html.twig', array(
            'user' => $user,
            'recruteur' => $recruteur
        ));

    }

    public function aproposAction($recruteur)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('UserBundle:UserRecruteur')->find([
            'id' => $recruteur
        ]);
        return $this->render('profile/showRecruteur_Fn/apropos.html.twig', [
            'user' => $user,
        ]);
    }

}