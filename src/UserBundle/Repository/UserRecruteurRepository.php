<?php

namespace UserBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * UserRecruteurRepository
 *
 * This class was generated by the PhpStorm "Php Annotations" Plugin. Add your own custom
 * repository methods below.
 */
class UserRecruteurRepository extends EntityRepository
{
    public function findRecByDomaine($requestString)
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT rec
                FROM UserBundle:UserRecruteur rec
                WHERE rec.domaine LIKE :str'
            )
            ->setParameter('str', array('%' . $requestString . '%'))
            ->getResult();

    }
}
